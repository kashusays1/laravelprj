<?php
namespace App\Modules\Offers\Services;

use App\Modules\Offers\Models\Offer;

class OffersService
{

  public static function getOffersListForAdmin($sort_order, $status, $keyword)
  {
    $offers = Offer::withOrderBy($sort_order)->withStatus($status);

    if ($keyword) {
        $offers = $offers->withSearchKeywords($keyword);
    }

    return $offers;
  }//getOffersListForAdmin

  public static function deleteOffer($offer_id)
  {
    $offer = offer::find($offer_id);
    //$offer->delete();
    return $offer->delete();
  }//deleteOffer

}
