<?php

namespace App\Modules\Orders\Http\Controllers;

use App\Core\MyBaseApiController;
use App\Modules\Orders\Models\ShopOrder;
use App\Modules\Orders\Transformers\OrderTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class OrdersApiController extends MyBaseApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request) {
        try {
            $orders = ShopOrder::canRateOrder()
                               ->where('user_id', Auth::user()->id)
                               ->orderBy('created_at', 'desc')
                               ->paginate($request->has('per_page') ? $request->per_page : env('PAGINATE_PER_PAGE'));

            $data = OrderTransformer::collection($orders);

            return $this->successResponseWithDataPaginated($data);
        } catch (\Exception $e) {
            if (app()->environment('local')) {
                $message = $e->getMessage() . ' file : ' . $e->getFile() . ' In Line : ' . $e->getLine();
            } else {
                $message = trans('common.Something Went Wrong');
            }
            return $this->errorResponse($message);
        }
    }
}
