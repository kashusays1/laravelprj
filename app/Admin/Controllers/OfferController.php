<?php
namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Offers\Services\OffersService;

class OfferController extends Controller
{

  public function index()
  {
    $status = request('active', 1);
    $data = [
        'title' => trans('offers.admin.page_title'),
        'sub_title' => '',
        'icon' => 'fa fa-indent'
    ];

    $data['listTh'] = $this->returnListTh();
    $sort_order = request('sort_order') ?? 'id__desc';

    $keyword = request('keyword') ?? '';

    $arrSort = $this->returnSortArray();
    $offers  = OffersService::getOffersListForAdmin($sort_order, $status, $keyword);

    $dataTmp = $offers->paginate(20);
    $data['dataTr'] = $this->returnTableTr($dataTmp);
    $data['pagination'] = $dataTmp->appends(request()->except(['_token', '_pjax']))->links('admin.component.pagination');
    $data['result_items'] = trans('offers.admin.result_item', ['item_from' => $dataTmp->firstItem(), 'item_to' => $dataTmp->lastItem(), 'item_total' => $dataTmp->total()]);

    $data['menu_left'] = $this->returnLeftMenu();

    $data['menu_right'] = $this->returnRightMenu();

    $optionSort = $this->returnSortArrayOptions($sort_order);

    $data['menu_sort'] = $this->returnMenuSort($optionSort);

    $data['script_sort'] = $this->returnScriptSort();

    $data['menu_search'] = $this->returnMenuSearch($keyword);

    $data['url_delete_item'] = route('admin_offers.delete');

    return view('admin.screen.offers.list')
        ->with($data);
  }//index

  private function returnListTh()
  {
    return [
        'id' => trans('offers.admin.table_header.id'),
        'code' => trans('offers.admin.table_header.code'),
        'type' => trans('offers.admin.table_header.type'),
        'value' => trans('offers.admin.table_header.value'),
        'apply_to' => trans('offers.admin.table_header.apply_to'),
        'start_date' => trans('offers.admin.table_header.start_date'),
        'end_date' => trans('offers.admin.table_header.end_date'),
        'status' => trans('offers.admin.table_header.status'),
        'action' => trans('offers.admin.table_header.action'),
    ];
  }//returnListTh

  private function returnTableTr($dataTmp)
  {
    $dataTr = [];
    foreach ($dataTmp as $key => $row) {
        $dataTr[] = [
            'id' => $row['id'],
            'code' => $row['code'],
            'type' => $row['type'],
            'value' => $row['value'],
            'apply_to' => $row['apply_to'],
            'start_date' => date('d-m-Y', strtotime($row['start_date'])),
            'end_date' => date('d-m-Y', strtotime($row['end_date'])),
            'status' => $row['status'] ? '<span class="label label-success">ON</span>' : '<span class="label label-danger">OFF</span>',
            'action' => '
                <a href="' . route('admin_store.edit', ['id' => $row['id']]) . '"><span title="' . trans('store.admin.edit') . '" type="button" class="btn btn-flat btn-primary"><i class="fa fa-edit"></i></span></a>&nbsp;
                <a href="' . route('admin_store.seller_details', ['id' => $row['id']]) . '"><span title="' . trans('store_info.store_info') . '" type="button" class="btn btn-flat btn-primary"><i class="fa fa-eye"></i></span></a>&nbsp;

              <span onclick="deleteItem(' . $row['id'] . ');"  title="' . trans('store.admin.delete') . '" class="btn btn-flat btn-danger"><i class="fa fa-trash"></i></span>
              ',
        ];
    }
    return $dataTr;
  }//returnTableTr

  private function returnSortArray()
  {
    return [
        'id__desc' => trans('offers.admin.sort_order.id_desc'),
        'id__asc' => trans('offers.admin.sort_order.id_asc')
    ];
  }//returnSortArray

  private function returnSortArrayOptions($sort_order)
  {
    $arrSort = $this->returnSortArray();
    $optionSort = '';
    foreach ($arrSort as $key => $status) {
        $optionSort .= '<option  ' . (($sort_order == $key) ? "selected" : "") . ' value="' . $key . '">' . $status . '</option>';
    }
    return $optionSort;
  }//returnSortArrayOptions

  private function returnLeftMenu()
  {
    return '<div class="pull-left">
                  <a class="btn   btn-flat btn-primary grid-refresh" title="Refresh"><i class="fa fa-refresh"></i><span class="hidden-xs"> ' . trans('offers.admin.refresh') . '</span></a> &nbsp;
                  </div>';
  }//returnLeftMenu

  private function returnRightMenu()
  {
    return '<div class="btn-group pull-right" style="margin-right: 10px">
                       <a href="' . route('admin_store.create') . '" class="btn  btn-success  btn-flat" title="New" id="button_create_new">
                       <i class="fa fa-plus"></i><span class="hidden-xs">' . trans('offers.admin.add_new') . '</span>
                       </a>
                    </div>';
  }//returnRightMenu

  private function returnMenuSort($optionSort)
  {
    return '<div class="btn-group pull-left">
                    <div class="form-group">
                       <select class="form-control" id="order_sort">
                        ' . $optionSort . '
                       </select>
                     </div>
                   </div>

                   <div class="btn-group pull-left">
                       <a class="btn btn-flat btn-primary" title="Sort" id="button_sort">
                          <i class="fa fa-sort-amount-asc"></i><span class="hidden-xs"> ' . trans('admin.sort') . '</span>
                       </a>
                   </div>';
  }//returnMenuSort

  private function returnScriptSort()
  {
    return "$('#button_sort').click(function(event) {
          var url = '" . route('admin_offers.index') . "?sort_order='+$('#order_sort option:selected').val();
          $.pjax({url: url, container: '#pjax-container'})
        });";
  }//returnScriptSort

  private function returnMenuSearch($keyword)
  {
    return '<form action="' . route('admin_offers.index') . '" id="button_search">
               <div onclick="$(this).submit();" class="btn-group pull-right">
                       <a class="btn btn-flat btn-primary" title="Refresh">
                          <i class="fa  fa-search"></i><span class="hidden-xs"> ' . trans('admin.search') . '</span>
                       </a>
               </div>
               <div class="btn-group pull-right">
                     <div class="form-group">
                       <input type="text" name="keyword" class="form-control" placeholder="' . trans('offers.admin.search_place') . '" value="' . $keyword . '">
                     </div>
               </div>
            </form>';
  }//returnMenuSearch

  /*
    Delete list item
    Need mothod destroy to boot deleting in model
   */

  public function delete()
  {
      if (!request()->ajax()) {
          return response()->json(['error' => 1, 'msg' => 'Method not allow!']);
      } else {
          $id = request('ids');
          $delete = OffersService::deleteOffer($id);
          return response()->json(['error' => 0, 'msg' => 'Order is successfully deleted.']);
      }
  }


}
