<?php
$router->group(['prefix' => 'offers'], function ($router) {
    $router->get('/', 'OfferController@index')->name('admin_offers.index');
    $router->post('/delete', 'OfferController@delete')->name('admin_offers.delete');
});
