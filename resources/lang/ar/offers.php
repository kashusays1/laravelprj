<?php

return [
  'api' =>  [
    'not_valid' => 'هذا العرض غير صالح بعد الآن.',
    'not_valid_for_seller' => 'هذا العرض غير صالح للبائع.',
  ],
  'admin' => [
    'menu' => [
      'offer_management' => 'Offers Management',
      'offer_list' => 'Active Offers',
    ],
    'page_title' => 'Offers List',
    'sort_order'     => [
        'id_asc'     => 'ID asc',
        'id_desc'    => 'ID desc'
    ],
    'refresh' => 'Refresh',
    'add_new' => 'Add new',
    'search_place'   => 'Search Code',
    'result_item' => 'Showing <b>:item_from</b> to <b>:item_to</b> of <b>:item_total</b> items</b>',
    'table_header' => [
      'id' => 'ID',
      'code' => 'Code',
      'image' => 'Image',
      'type' => 'Type',
      'value' => 'Value',
      'apply_to' => 'Apply To',
      'start_date' => 'Start Date',
      'end_date' => 'End Date',
      'status' => 'Status',
      'action' => 'Action',
    ],
  ]

];
