@extends('mail.layout')

@section('main')
<h3>Received a feedback </h3>
<p>Feedback ID: {{$id}} </p>
<p>Feedback created at: {{$created_at}} </p>
<p>Sender Name: {{$customer_name}} </p>
<p>Sender Email: {{$customer_email}} </p>

@endsection
